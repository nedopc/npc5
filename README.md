# Project nedoPC-5

DIY personal computer built around 32-bit version of RISC-V processor

Planned soft core to use is Retro-V by Shaos:

https://gitlab.com/shaos/retro-v/tags/v1.0.0

# RISC-V RV32I[MA] emulator with ELF support

Emulator was originally created by Fabrice Bellard and then modified and shared on Hackaday by Frank Buss
as a single C-file. Shaos added some additional statistics and macros. How to compile it:

    gcc -O3 -Wall -lelf emu-rv32i.c -o emu-rv32i

Passed RV32I compliance tests from https://github.com/riscv/riscv-compliance

    make RISCV_TARGET=spike RISCV_DEVICE=rv32i TARGET_SIM=/full/path/emulator variant

Compiling and running simple code:

    riscv32-unknown-elf-gcc -O3 -nostdlib test1.c -o test1

or

    riscv64-unknown-elf-gcc -march=rv32i -mabi=ilp32 -O3 -nostdlib test1.c -o test1

then

    ./emu-rv32i test1
    Hello RISC-V!

RV32M and RV32A instructions may be enabled by commenting #define STRICT_RV32I

# How to build RISC-V toolchain

https://riscv.org/software-tools/risc-v-gnu-compiler-toolchain/

Latest one is GCC 8.2.0

64-bit universal version (riscv64-unknown-elf-* that can build 32-bit code too):

    ./configure --prefix=/opt/riscv
    make

32-bit version (riscv32-unknown-elf-*):

    ./configure --prefix=/opt/riscv32 --with-arch=rv32i --with-abi=ilp32
    make

